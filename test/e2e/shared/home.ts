import { IdentificationType, CommonTasks } from './common-tasks';

// Component Locators
export const LocatorsHome = {

    withdrawalButton: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/button[3]'
    },

    amountTextBox: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/label[contains(text(), "Amount to be Withdrawn")]/following-sibling::input'
    },

    withdrawButton: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/form[1]/button[1]'
    },

    transactionSuccessfulText: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/span[contains(text(), "Transaction successful")]'
    },

    depositSuccessfulText: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/span[text()="Deposit Successful"]'
    },

    depositButton: {
        type: IdentificationType[IdentificationType.Xpath],
        value: '//button[contains(text(), "Deposit")][contains(@ng-class, "btnClass2")]'
    },

    completeDepositButton: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/form[1]/button[1]'
    },

    depositAmountTextBox: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/label[contains(text(), "Amount to be Deposited")]/following-sibling::input'
    }
    
}

export class HomePage extends CommonTasks {

    // withdrawal Button
    withdrawalButton = this.elementLocator(LocatorsHome.withdrawalButton);

    // Amount
    amountTextBox = this.elementLocator(LocatorsHome.amountTextBox);

    // Withdraw
    withdrawButton = this.elementLocator(LocatorsHome.withdrawButton);

    // Transaction Successful
    transactionSuccessfulText = this.elementLocator(LocatorsHome.transactionSuccessfulText);

    // DepositButton
    depositButton = this.elementLocator(LocatorsHome.depositButton);

    // Complete Deposit Button
    completeDepositButton = this.elementLocator(LocatorsHome.completeDepositButton);

    // Deposit Successful Text
    depositSuccessfulText = this.elementLocator(LocatorsHome.depositSuccessfulText);

    // Deposit Amount Text Box
    depositAmountTextBox = this.elementLocator(LocatorsHome.depositAmountTextBox);
}
