exports.config = {
  capabilities: {
    'browserName': 'chrome',
    chromeOptions: {
      args: ['disable-infobars', 'start-maximized'],
    },
    metadata: {
      device: 'Dell Latitude 7280'
    }
  },
  directConnect: true,
  //baseUrl: 'http://localhost:4200',
  baseUrl: 'http://www.way2automation.com/angularjs-protractor/banking/#/login/',

  specs: [
    './e2e/**/*.feature',
  ],

  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),

  cucumberOpts: {
    require: ['./e2e/**/*.ts', './hooks/**/*.ts',],
    tags: ["@demo"],
    strict: true,
    format: [
      'progress',
      'json:./test/report/results.json'
    ],
    dryRun: false,
    compiler: [],
  },

  plugins: [{
    package: 'protractor-multiple-cucumber-html-reporter-plugin',
    options: {
      automaticallyGenerateReport: true,
      removeExistingJsonReportFile: true,
      openReportInBrowser: true,
      removeOriginalJsonReportFile: true,
      removeExistingJsonReportFile: true
    }
  }],

  onPrepare() {
    // Override the timeout for webdriver.
    browser.manage().timeouts().setScriptTimeout(120000);
    browser.rootElement = 'app-root'
    require('ts-node').register({
      project: './test/tsconfig.e2e.json'
    });
  },
  onComplete() {
    browser.driver.quit();
  }
};
