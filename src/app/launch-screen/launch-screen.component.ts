import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { CustomerInformation } from './../contract';
import { IMAGE_PATH } from './../shared';
import { LaunchService } from './../shared/services/launch-screen/launch.service';

@Component({
  selector: 'app-launch-screen',
  templateUrl: './launch-screen.component.html',
  styleUrls: ['./launch-screen.component.scss']
})
export class LaunchScreenComponent implements OnInit {
  // This component will be deleted as these values will be sent to the CPP application via external system.
  public launchForm: FormGroup;
  public contractNameModel: string;
  public contractTypeModel: string;
  public contractStartDtModel: NgbDateStruct;
  public contractEndDtModel: NgbDateStruct;
  public imageDir = IMAGE_PATH;
  private subscription: Subscription;
  private cppId: number;

  public contractTypes = [
    { contractTypeId: 'DAN', longDescription: 'Distribution Agreement National' },
    { contractTypeId: 'DAR', longDescription: 'Distribution Agreement Regional' },
    { contractTypeId: 'IFS', longDescription: 'Independent Food Service (IFS)' }
  ];

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _ngbDateParserFormatter: NgbDateParserFormatter,
    private _launchService: LaunchService
  ) {}

  ngOnInit() {
    this.loadForm();
  }

  loadForm() {
    this.launchForm = this._formBuilder.group({
      contractName: new FormControl(null, Validators.required),
      contractType: new FormControl(null, Validators.required),
      contractStartDate: new FormControl(null, Validators.required),
      contractEndDate: new FormControl(null, Validators.required)
    });
  }

  buildParamsObject() {
    const paramsObject = {
      cname: this.contractNameModel.trim(),
      ctype: this.contractTypeModel,
      stdate: this._ngbDateParserFormatter.format(this.contractStartDtModel),
      enddate: this._ngbDateParserFormatter.format(this.contractEndDtModel),
      cppid: this.cppId
    };
    return paramsObject;
  }

  onSubmit() {
    this.subscription = this._launchService.getCustomerId().subscribe(customerInformation => {
      this.cppId = customerInformation.contractPriceProfileId;
      const objectParams = this.buildParamsObject();
      this._router.navigate(['/pricinginformation'], { queryParams: objectParams });
    });
  }
}
