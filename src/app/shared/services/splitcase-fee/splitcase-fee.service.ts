import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Params } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { apiUrls } from './../app.url';
import { SplitCaseFees } from './../../../contract';

@Injectable()
export class SplitcaseFeeService {

  private fetchSplitcaseFeeURL = apiUrls.fetchSplitcaseFeeURL;
  private splitcaseFeeSaveURL = apiUrls.splitCaseSaveURL;


  constructor(private http: HttpClient) { }

  public fetchProductTypes(cppId: string, effectiveDate: string, expirationDate: string): Observable<any> {
    let params = new HttpParams().set('contractPriceProfileId', cppId);
    params = params.set('pricingEffectiveDate', effectiveDate);
    params = params.set('pricingExpirationDate', expirationDate) ;
    return this.http
    .get(this.fetchSplitcaseFeeURL, {params})
    .map(res => res);
}

 public saveSplitCaseFee(splitCaseList): Observable<any> {
   return this.http.post(this.splitcaseFeeSaveURL, splitCaseList);
 }

}
