export const apiUrls = {
  distributionCenterURL : 'rest/distributionCenter/distributionCentersForCompany',
  distributionCenterSaveURL : 'rest/distributionCenter/saveDistributionCenters',
  distributionCentersFetchURL : 'rest/distributionCenter/fetchSavedDistributionCenters',
  fetchCustomerIdURL : 'rest/customer/fetchCustomerId',
  savePricingInformationURL : 'rest/contractPricing/savePricingInformation',
  fetchCPPSequenceURL : 'rest/contractPricing/fetchCPPSequence',
  fetchMarkupGridURL : 'rest/markup/fetchMarkupDefaults',
  saveMarkupGridURL : 'rest/markup/saveMarkup',
  addExceptionURL : 'rest/markup/addException',
  renameMarkupExceptionURL : 'rest/markup/renameMarkupException',
  deleteMarkupExceptionURL : 'rest/markup/deleteMarkupException',
  saveMarkupOnSellURL : 'rest/markup/saveMarkupOnSell',
  fetchExceptionDefaultURL : 'rest/markup/fetchExceptionDefault',
  fetchSplitcaseFeeURL : 'rest/splitcase/fetchSplitCase',
  splitCaseSaveURL : 'rest/splitcase/saveSplitCaseFee',
  fetchReviewDataURL : 'rest/review/fetchReviewData'
};
