import { Component, OnInit, Input, Output, EventEmitter, Renderer2, ViewChild } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';

import { IMAGE_PATH, MARKUP_TYPES, EXPIRE_LOWER, UNIT_TYPES, BACKSPACE_KEY, MIN_MARKUP_DOLLAR, MAX_MARKUP_DOLLAR, MIN_MARKUP_PERCENT,
   MAX_MARKUP_PERCENT, MARKUP_PATTERN, ZERO_VALUE, BTN_MARKUP_SAVED, BTN_SAVE_MARKUP } from 'app/shared';
import { MarkupGridModel, MarkupGridDetails } from './markup-grid.model';

@Component({
  selector: 'app-markup-grid',
  templateUrl: './markup-grid.component.html',
  styleUrls: ['./markup-grid.component.scss']
})
export class MarkupGridComponent implements OnInit {
  @Input() rows: MarkupGridModel[];
  @Input() index = 0;
  @Input() expireLowerVal = false;
  @Input() markupGridDetails: MarkupGridDetails;
  @Output() saveMarkupGridDetails = new EventEmitter<MarkupGridDetails>();
  @Output() editMarkupGrid = new EventEmitter<number>();
  @Output() onUnitChangeVal  = new EventEmitter<string>();

  public selectedRows: MarkupGridModel[] = [];
  public markupGridForm: FormGroup;
  public expireLowerLabel = EXPIRE_LOWER;
  public selected = [];
  public imageDir = IMAGE_PATH;
  public isSaveDisabled = true;

  constructor(
    private formBuilder: FormBuilder,
    private _renderer: Renderer2
  ) {}

  ngOnInit() {
    this.loadForm();
  }

  loadForm() {
    this.markupGridForm = this.formBuilder.group({
      expireLowerToggle: this.formBuilder.group({
        selectedValue: [false]
      })
    });
  }

  checkIsDisabled() {
    return this.isSaveDisabled = this.rows.some(element => !element.markup);
  }

  onMarkupKeyUp($event, row) {
    row.markup = $event.target.value;
    this.markupGridDetails.isCompleted = false;
    this.editMarkupGrid.emit(this.markupGridDetails.markupId);
  }

  onMarkupKeyPress($event, row) {
    let inputChar = String.fromCharCode($event.charCode);
    const updatedValue = ($event.target.value + inputChar).split('.');
    if (($event.keyCode !== BACKSPACE_KEY && !MARKUP_PATTERN.test(inputChar) ) ||
     (updatedValue[1] && updatedValue[1].length > 2) || (updatedValue[0] && updatedValue[0].length > 2) || updatedValue.length > 2) {
      $event.preventDefault();
    }
  }

  onMarkupTypeChange(event, row) {
    row.markupType = event.target.value;
    this.markupGridDetails.isCompleted = false;
    this.editMarkupGrid.emit(this.markupGridDetails.markupId);
  }

  onUnitChange(event, row) {
    row.unit = event.target.value;
    if (event.target.value === UNIT_TYPES.PERCENT) {
      this.onUnitChangeVal.emit(row.unit);
      row.markupType = MARKUP_TYPES.SELL_UNIT;
      this._renderer.setAttribute(event.target.parentNode.offsetParent.children[3].querySelector('.markup-type'), 'disabled', 'true');
    } else {
      this.onUnitChangeVal.emit(row.unit);
      this._renderer.removeAttribute(event.target.parentNode.offsetParent.children[3].querySelector('.markup-type'), 'disabled');
    }
    this.markupGridDetails.isCompleted = false;
    this.editMarkupGrid.emit(this.markupGridDetails.markupId);
  }

  onblurMarkup(event, row) {
    if (event.target.value === '.') {
      row.markup = ZERO_VALUE;
      this._renderer.setProperty(event.target, 'value', ZERO_VALUE);
    } else {
      let markupVal = event.target.value ? DecimalPipe.prototype.transform(event.target.value, '1.2-2') : '';
      row.markup = markupVal.replace(/,/g, '');
      this._renderer.setProperty(event.target, 'value', markupVal.replace(/,/g, ''));
    }
  }

  copyRow(event, row) {
    this.rows.forEach((ele, index) => {
      const i = (this.index * this.rows.length) + index;
      ele.markup = row.markup;
      ele.unit = row.unit;
      ele.markupType = row.markupType;
      if (row.unit === UNIT_TYPES.PERCENT) {
        this._renderer.setAttribute(document.getElementsByClassName('markup-type')[i], 'disabled', 'true');
      } else {
        this._renderer.removeAttribute(document.getElementsByClassName('markup-type')[i], 'disabled');
      }
    });
    this.markupGridDetails.isCompleted = false;
    this.onUnitChangeVal.emit(row.unit);
    this.editMarkupGrid.emit(this.markupGridDetails.markupId);
  }

  validateData(row) {
    if (row.markup !== '' &&
      ((row.unit === UNIT_TYPES.DOLLAR && (row.markup) < MIN_MARKUP_DOLLAR) ||
        (row.unit === UNIT_TYPES.DOLLAR && (row.markup) > MAX_MARKUP_DOLLAR) ||
        (row.unit === UNIT_TYPES.PERCENT && (row.markup) < MIN_MARKUP_PERCENT) ||
        (row.unit === UNIT_TYPES.PERCENT && (row.markup) > MAX_MARKUP_PERCENT)) ) {
      return true;
    } else {
      return false;
    }
  }

  onChangeExpireLower(selectedValue) {
    this.expireLowerVal = selectedValue;
    this.markupGridDetails.isCompleted = false;
    this.editMarkupGrid.emit(this.markupGridDetails.markupId);
  }

  onSubmit($event) {
    this.markupGridDetails.markupList = this.rows;
    this.markupGridDetails.expireLower = this.expireLowerVal;
    this.markupGridDetails.isCompleted = true;
    this.saveMarkupGridDetails.emit(this.markupGridDetails);
  }
}
