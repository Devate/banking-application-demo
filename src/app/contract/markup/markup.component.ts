import { Component, OnInit, OnDestroy, Renderer2, ViewChildren, ElementRef, QueryList, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { IMAGE_PATH, MARKUP_ON_SELL, CONTRACT_TYPES, STEPPER_URL, MAX_MARKUP_EXCEPTIONS, UNIT_TYPES, ONE } from './../../shared';
import { MarkupGridModel, MarkupGridDetails, SaveMarkupGridModel, ExceptionModel,
   SaveMarkupOnSellModel, RenameMarkupModel } from './markup-grid/markup-grid.model';
import { MarkupService, ToasterService, TranslatorService } from './../../shared';
import { StepperService, STEPPER_NUMBERS } from './../../shared';
declare var $: any;

@Component({
  selector: 'app-markup',
  templateUrl: './markup.component.html',
  styleUrls: ['./markup.component.scss'],
})
export class MarkupComponent implements OnInit, OnDestroy  {

  @ViewChildren('copy') private copySelects: QueryList<ElementRef>;
  @ViewChild('editModalTitle') private editModalTitle: ElementRef;
  @ViewChild('editModalInput') private editModalInput: ElementRef;
  @ViewChild('deleteModalTitle') private deleteModalTitle: ElementRef;

  public markupForm: FormGroup;
  public markupModalForm: FormGroup;
  public editmarkupModalForm: FormGroup;
  public imageDir = IMAGE_PATH;
  public markupOnSellLabel = MARKUP_ON_SELL;
  public defaultToggleOn = true;
  public markupOnSellVal = true;
  public showMarkupOnSell: boolean;
  public copyMarkups = [];
  private subscription: Subscription[] = [];
  public selectedContractType: string;
  public contractTypeIFS = CONTRACT_TYPES.IFS;
  public contractPriceProfileId: any;
  public effectiveDate: string;
  public expirationDate: string;
  public contractName: string;
  public isNextDisabled = true;
  public isAddExecptionVisible = true;
  public markupAccordions: MarkupGridDetails[] = [];
  public saveMarkupGridModel = <SaveMarkupGridModel>{};
  public exceptionModel = <ExceptionModel>{};
  public saveMarkupOnSellModel = <SaveMarkupOnSellModel>{};
  public renameMarkupModel = <RenameMarkupModel>{};

  constructor(
    private _stepperService: StepperService,
    private _formBuilder: FormBuilder,
    private _markupService: MarkupService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _renderer: Renderer2,
    private _toaster: ToasterService,
    private _translatorService: TranslatorService
  ) {}

  ngOnInit() {
    this.loadForm();
    this.loadMarkupModalForm();
    this.loadEditMarkupModalForm();
    this._stepperService.currentStep(STEPPER_NUMBERS.MARKUP);
    this.fetchGridDetails();
    if (this.selectedContractType === CONTRACT_TYPES.IFS) {
      this._stepperService.contractTypeChange(this.selectedContractType);
    }
  }

  loadForm() {
    this.markupForm = this._formBuilder.group({
      markupOnSellToggle: this._formBuilder.group({
        selectedValue: [false]
      })
    });
  }

  loadMarkupModalForm() {
    this.markupModalForm = new FormGroup({
      markupStructure: new FormControl(null, [Validators.required, this.markupNameValidator.bind(this)]),
    });
  }

  loadEditMarkupModalForm() {
    this.editmarkupModalForm = new FormGroup({
      editMarkupStructure: new FormControl(null, [Validators.required, this.markupNameValidator.bind(this)]),
    });
  }

  markupNameValidator(control: FormControl) {
    const input = control.value;
    const isWhitespace = (input || '').trim().length === 0;
    const isValid = !isWhitespace;
      if (!isValid) {
        return { 'whitespace' : true };
      } else if (input && this.markupAccordions.some(element => (element.markupName).toLowerCase() == (input.toLowerCase()).trim())) {
        return { 'duplicateMarkupName': true };
      } else {
        return null;
      }
  }

  checkAddExecption() {
    if (this.markupAccordions.length >= MAX_MARKUP_EXCEPTIONS ) {
      this.isAddExecptionVisible = false;
    }
  }

  fetchGridDetails() {
    const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
    this.contractPriceProfileId = contractDetails.cppseqid;
    this.contractName = contractDetails.cname;
    this.effectiveDate = contractDetails.cstdate;
    this.expirationDate = contractDetails.cenddate;
    this.selectedContractType = contractDetails.ctype;
    this.subscription.push(this._markupService.getMarkupGridDetails(this.contractPriceProfileId, this.effectiveDate,
      this.expirationDate, this.contractName)
    .subscribe(response => {
      this.markupAccordions = response;
      this.checkAddExecption();
    }));
  }

  onEditMarkupGrid(editMarkupId) {
    this.isNextDisabled = this.markupAccordions.some(element => !element.isCompleted);
    let editCopy = this.copyMarkups.some(markup => markup.markupId == editMarkupId);
    if (editCopy) {
      this.copySelects.map(copyDropdown => {
        if (copyDropdown.nativeElement.value == editMarkupId) {
          copyDropdown.nativeElement.value = '';
        }
      });
      this.copyMarkups.forEach((markups, index) => {
        if (markups.markupId == editMarkupId) {
              let removeIndex = this.copyMarkups.indexOf(markups);
              this.copyMarkups.splice(removeIndex, 1);
        }
      });
    }
  }

  onSaveMarkupGridDetails(saveMarkupGridDetails) {
    this.saveMarkupGridModel.contractPriceProfileSeq =  this.contractPriceProfileId;
    this.saveMarkupGridModel.markupWrapper = saveMarkupGridDetails;
    this.subscription.push(this._markupService.saveMarkupGridDetails(this.saveMarkupGridModel)
    .subscribe(response => {
      this.isNextDisabled = this.markupAccordions.some(element => !element.isCompleted);
      let copyPush = this.copyMarkups.some(ele => ele.markupId == saveMarkupGridDetails.markupId);
      if (!copyPush) {
        this.copyMarkups.push({markupName: saveMarkupGridDetails.markupName, markupId: saveMarkupGridDetails.markupId});
      }
    }));
  }

  navigateToUrl() {
    if (this.selectedContractType !== CONTRACT_TYPES.IFS) {
      this._router.navigate([STEPPER_URL.SPLIT_CASE_URL], {relativeTo: this._route});
    } else {
      this._router.navigate([STEPPER_URL.REVIEW_URL], {relativeTo: this._route});
    }
  }

  buildMarkupOnSellDetails() {
    this.saveMarkupOnSellModel.contractPriceProfileSeq = this.contractPriceProfileId;
    this.saveMarkupOnSellModel.effectiveDate = this.effectiveDate;
    this.saveMarkupOnSellModel.expirationDate = this.expirationDate;
    this.saveMarkupOnSellModel.markupOnSell = this.markupOnSellVal;
  }

  onSubmit() {
    this.buildMarkupOnSellDetails();
    this.subscription.push(this._markupService.saveMarkupOnSell(this.saveMarkupOnSellModel)
    .subscribe(response => {
      this.navigateToUrl();
    }));
  }

  toggleCollapse($event) {
    const element = $event.target.parentNode.querySelector('.fa');
    if (element.classList[1] === 'fa-angle-down') {
      this._renderer.removeClass(element, 'fa-angle-down');
      this._renderer.addClass(element, 'fa-angle-right');
    } else {
      this._renderer.removeClass(element, 'fa-angle-right');
      this._renderer.addClass(element, 'fa-angle-down');
    }
  }

  onAddException() {
    const exceptionName = (this.markupModalForm.get('markupStructure').value).trim();
    this.exceptionModel.contractPriceProfileSeq = this.contractPriceProfileId;
    this.exceptionModel.exceptionName = exceptionName;
    this.subscription.push(this._markupService.addExceptionDetails(this.exceptionModel)
    .subscribe(() => {
      this.subscription.push(this._markupService.getExceptionDefaults(this.contractPriceProfileId, this.effectiveDate,
        this.expirationDate, exceptionName)
        .subscribe(response => {
          this.markupAccordions.push(response[0]);
          this.isNextDisabled = true;
          this.checkAddExecption();
          this.markupModalForm.reset();
        }));
    }));
  }

  onCancelException() {
    this.markupModalForm.reset();
    this.editmarkupModalForm.reset();
  }

  onCopyFrom($event) {
    let copyFromGrid = [];
    let currentIndex = $event.target.getAttribute('copyIndex');
    let markups = this.markupAccordions[currentIndex];
    let markupId = $event.target.value;
    this.markupAccordions.forEach((markupGridDetails) => {
      if (markupGridDetails.markupId == markupId) {
        copyFromGrid = markupGridDetails.markupList;
      }
    });
    copyFromGrid.forEach((element, index) => {
      let ele = markups.markupList[index];
      ele.markup = element.markup;
      ele.unit = element.unit;
      ele.markupType = element.markupType;
    });
    markups.isCompleted = false;
  }

  toggleMarkupOnSell(selectedValue) {
    this.markupOnSellVal = selectedValue;
  }

  scanGridforPercentCount() {
    let percentCount = 0;
     this.markupAccordions.forEach((ele) => {
       ele.markupList.forEach((gridData) => {
          if (gridData.unit === UNIT_TYPES.PERCENT) {
            percentCount++;
          }
       });
     });
     return percentCount;
  }

  setMarkupOnSell(value) {
    if (value === UNIT_TYPES.PERCENT) {
      this.showMarkupOnSell = true;
      if (this.scanGridforPercentCount() === ONE) {
        this.markupOnSellVal = true;
      }
    } else {
      this.showMarkupOnSell = this.scanGridforPercent();
      if (!this.showMarkupOnSell) {
        this.markupOnSellVal = false;
      }
    }
  }

  scanGridforPercent() {
    return this.markupAccordions.map((element) => {
      return element.markupList.some(ele => ele.unit === UNIT_TYPES.PERCENT)
    }).some(e => e);
  }

  onEdit($event) {
    const editTitle = this._translatorService.translate('MARKUP_LABELS.EDIT');
    let markupName = $event.target.getAttribute('markupName');
    let markupId = $event.target.getAttribute('markupId');
    this.editModalTitle.nativeElement.textContent = editTitle + ' "' + markupName + '"';
    this._renderer.setAttribute(this.editModalInput.nativeElement, 'markupId', markupId);
    this._renderer.setAttribute(this.editModalInput.nativeElement, 'markupName', markupName);
  }

  onRenameMarkupException() {
    let selectedMarkupId = this.editModalInput.nativeElement.getAttribute('markupId');
    let selectedMarkupName = this.editModalInput.nativeElement.getAttribute('markupName');
    let newMarkupName = (this.editmarkupModalForm.get('editMarkupStructure').value).trim();
    this.renameMarkupModel.contractPriceProfileSeq = this.contractPriceProfileId;
    this.renameMarkupModel.markupId = selectedMarkupId;
    this.renameMarkupModel.markupName = selectedMarkupName;
    this.renameMarkupModel.newMarkupName = newMarkupName;

    this.subscription.push(this._markupService.renameMarkupException(this.renameMarkupModel)
    .subscribe(() => {
      this.markupAccordions.forEach((markupGridDetails) => {
        if (markupGridDetails.markupId == selectedMarkupId) {
          markupGridDetails.markupName = newMarkupName;
          this.editmarkupModalForm.reset();
        }
      });
    }));
  }

  onDeleteClick() {
    const deleteTitle = this._translatorService.translate('MARKUP_LABELS.DELETE');
    $('#editmarkupModal').modal('hide');
    let toDeletemarkupName = this.editModalInput.nativeElement.getAttribute('markupName');
    this.deleteModalTitle.nativeElement.textContent = deleteTitle + ' "' + toDeletemarkupName + '"?';
  }

  onDeleteException() {
    const deleteSuccessMsg = this._translatorService.translate('TOASTER_MESSAGES.DELETE_SUCCESS');
    let deletemarkupId = this.editModalInput.nativeElement.getAttribute('markupId');
    let deletemarkupName = this.editModalInput.nativeElement.getAttribute('markupName');
    this.subscription.push(this._markupService.deleteMarkupException(this.contractPriceProfileId, deletemarkupId, deletemarkupName)
    .subscribe(() => {
      this.markupAccordions.forEach((markupGridDetails, index) => {
        if (markupGridDetails.markupId == deletemarkupId) {
          this.markupAccordions.splice(index, 1);
          this.showMarkupOnSell = this.scanGridforPercent();
          if (!this.showMarkupOnSell) {
            this.markupOnSellVal = false;
          }
          this.onEditMarkupGrid(deletemarkupId);
          this._toaster.showSuccess('"' + deletemarkupName + '"' + deleteSuccessMsg, '');
        }
      });
    }));
  }

  ngOnDestroy() {
    this.subscription.forEach(sub => sub.unsubscribe());
  }

}
