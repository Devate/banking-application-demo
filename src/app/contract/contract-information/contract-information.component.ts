import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';

import { CONTRACT_TYPES } from './../../shared/utils/app.constants';
import { StepperService } from '../../shared/services/stepper/stepper.service';

@Component({
  selector: 'app-contract-information',
  templateUrl: './contract-information.component.html',
  styleUrls: ['./contract-information.component.scss']
})
export class ContractInformationComponent implements OnInit {
  private _contractInfoInd: boolean;
  @Input() public contractInfoInd;

  public contractName: string;
  public contractType: string;
  public contractStartDate: string;
  public contractEndDate: string;
  public priceStartDate = '';
  public priceEndDate: string;
  public cppId: number;

  constructor(
    private _route: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private _stepperService: StepperService
  ) {}

  ngOnInit() {
    this.contractName = this._route.snapshot.queryParams['cname'];
    this.contractType = this._route.snapshot.queryParams['ctype'];
    this.contractStartDate = this._route.snapshot.queryParams['stdate'];
    this.contractEndDate = this._route.snapshot.queryParams['enddate'];
    this.priceEndDate = this._route.snapshot.queryParams['enddate'];
    this.cppId = this._route.snapshot.queryParams['cppid'];

    this.fetchContractInfo();
    this.applyContractType();

    this._stepperService.getCurrentStep().subscribe(stepNumber => {
      if (stepNumber > 1) {
        this.contractInfoInd = true;
        const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
        this.priceStartDate = contractDetails.pstdate;
      } else {
        this.contractInfoInd = false;
      }
    });
  }

  applyContractType() {
    if (this.contractType === CONTRACT_TYPES.SDAN) {
      this.contractType = CONTRACT_TYPES.DAN;
    } else if (this.contractType === CONTRACT_TYPES.SDAR) {
      this.contractType = CONTRACT_TYPES.DAR;
    } else if (this.contractType === CONTRACT_TYPES.IFS) {
      this.contractType = CONTRACT_TYPES.IF;
    } else {
      this.contractType = '';
    }
  }

  fetchContractInfo() {
    if (this.contractName) {
      const contractInformation = {
        cname: this.contractName,
        ctype: this.contractType,
        cstdate: this.contractStartDate,
        cenddate: this.contractEndDate,
        penddate: this.contractEndDate,
        cppid: this.cppId
      };
      sessionStorage.setItem('contractInfo', JSON.stringify(contractInformation));
    } else {
      const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
      this.contractName = contractDetails.cname;
      this.contractType = contractDetails.ctype;
      this.contractStartDate = contractDetails.cstdate;
      this.contractEndDate = contractDetails.cenddate;
      this.priceEndDate = contractDetails.cenddate;
    }
  }
}
